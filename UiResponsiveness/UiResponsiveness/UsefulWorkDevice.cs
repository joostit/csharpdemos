﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace UiResponsiveness
{
    public class UsefulWorkDevice
    {

        private const int sleepTime = 10000;

        public bool DoUsefulWork()
        {
            Thread.Sleep(sleepTime);

            return true;
        }


        public async Task<bool> DoUsefulWorkAsync()
        {

            // Add: async void
            // Add: async Task  --> Allows for retrieving the Task Properties which describe the async process. (isStarted etc)

            bool success = await Task<bool>.Run(() =>
            {
                return DoUsefulWork();
            });

            return success;
        }

    }
}
