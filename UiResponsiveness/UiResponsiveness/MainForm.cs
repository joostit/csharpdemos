﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UiResponsiveness
{
    public partial class MainForm : Form
    {

        #region BoilerPlate

        UsefulWorkDevice usefulWorkDevice = new UsefulWorkDevice();
        private const int counterInterval = 1000;
        private int counter = 0;

        System.Windows.Forms.Timer counterTimer = new Timer();

        public MainForm()
        {
            InitializeComponent();

            counterTimer.Tick += CounterTimer_Tick;
            counterTimer.Interval = counterInterval;
            counterTimer.Start();
        }

        private void CounterTimer_Tick(object sender, EventArgs e)
        {
            counter++;
            counterLabel.Text = counter.ToString();
        }

        #endregion




        private async void StartWorkButton_Click(object sender, EventArgs e)
        {
            statusLabel.Text = "Status: Working";
            startWorkButton.Enabled = false;

            bool result = await usefulWorkDevice.DoUsefulWorkAsync();

            statusLabel.Text = "Status: Done. Success = " + result.ToString();
            startWorkButton.Enabled = true;
        }





    }
}
